﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace projek
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void email_TextChanged(object sender, EventArgs e)
        {

        }

        private void passTextField_TextChanged(object sender, EventArgs e)
        {

        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            // Ambil data dari input form
            string email = emailTextField.Text;
            string password = passTextField.Text;
            string connectionString = ConfigurationManager.ConnectionStrings["projek.Properties.Settings.mahasiswa_dbConnectionString"].ConnectionString;


            // Buat koneksi ke database
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                // Query untuk memeriksa keberadaan data di database
                string query = "SELECT COUNT(*) FROM akun WHERE akun_email=@email AND akun_pass=@password";

                // Buat perintah SQL
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Tambahkan parameter untuk menghindari SQL injection
                    command.Parameters.AddWithValue("@email", email);
                    command.Parameters.AddWithValue("@password", password);

                    // Eksekusi query
                    int userCount = (int)command.ExecuteScalar();

                    // Cek hasil query
                    if (userCount > 0)
                    {
                        // Jika ada data yang cocok, tampilkan pesan dan buka form home
                        MessageBox.Show("Login berhasil!");

                        // Tutup form login
                        this.Close();

                        // Buka form home
                        Home homeForm = new Home();
                        homeForm.Show();
                    }
                    else
                    {
                        MessageBox.Show("Login gagal. Periksa kembali email dan password Anda.");
                    }
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
