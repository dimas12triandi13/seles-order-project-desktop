﻿namespace projek
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProdukBtn = new System.Windows.Forms.Button();
            this.OrderJualBtn = new System.Windows.Forms.Button();
            this.PenjualanBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.AkunBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ProdukBtn
            // 
            this.ProdukBtn.Location = new System.Drawing.Point(253, 158);
            this.ProdukBtn.Name = "ProdukBtn";
            this.ProdukBtn.Size = new System.Drawing.Size(104, 23);
            this.ProdukBtn.TabIndex = 1;
            this.ProdukBtn.Text = "Produk";
            this.ProdukBtn.UseVisualStyleBackColor = true;
            this.ProdukBtn.Click += new System.EventHandler(this.ProdukBtn_Click);
            // 
            // OrderJualBtn
            // 
            this.OrderJualBtn.Location = new System.Drawing.Point(473, 158);
            this.OrderJualBtn.Name = "OrderJualBtn";
            this.OrderJualBtn.Size = new System.Drawing.Size(104, 23);
            this.OrderJualBtn.TabIndex = 2;
            this.OrderJualBtn.Text = "Order Penjualan ";
            this.OrderJualBtn.UseVisualStyleBackColor = true;
            this.OrderJualBtn.Click += new System.EventHandler(this.OrderJualBtn_Click);
            // 
            // PenjualanBtn
            // 
            this.PenjualanBtn.Location = new System.Drawing.Point(141, 158);
            this.PenjualanBtn.Name = "PenjualanBtn";
            this.PenjualanBtn.Size = new System.Drawing.Size(104, 23);
            this.PenjualanBtn.TabIndex = 3;
            this.PenjualanBtn.Text = "Penjualan";
            this.PenjualanBtn.UseVisualStyleBackColor = true;
            this.PenjualanBtn.Click += new System.EventHandler(this.PenjualanBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(217, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(284, 108);
            this.label1.TabIndex = 4;
            this.label1.Text = "Menu";
            // 
            // AkunBtn
            // 
            this.AkunBtn.Location = new System.Drawing.Point(363, 158);
            this.AkunBtn.Name = "AkunBtn";
            this.AkunBtn.Size = new System.Drawing.Size(104, 23);
            this.AkunBtn.TabIndex = 0;
            this.AkunBtn.Text = "Akun";
            this.AkunBtn.UseVisualStyleBackColor = true;
            this.AkunBtn.Click += new System.EventHandler(this.AkunBtn_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PenjualanBtn);
            this.Controls.Add(this.OrderJualBtn);
            this.Controls.Add(this.ProdukBtn);
            this.Controls.Add(this.AkunBtn);
            this.Name = "Home";
            this.Text = "Home";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ProdukBtn;
        private System.Windows.Forms.Button OrderJualBtn;
        private System.Windows.Forms.Button PenjualanBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AkunBtn;
    }
}