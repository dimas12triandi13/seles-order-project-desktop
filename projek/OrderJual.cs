﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace projek
{
    public partial class OrderJual : Form
    {
        public OrderJual()
        {
            InitializeComponent();

        }

        private void Order_dateTimePicker_ValueChanged(object sender, EventArgs e)
        {

        }

        private void OrderBtn_Click(object sender, EventArgs e)
        {// Ambil tanggal dari DateTimePicker
            DateTime orderDate = Order_dateTimePicker.Value;
            string akun_id = textBox1.Text;

            // Mendapatkan string koneksi dari konfigurasi
            string connectionString = ConfigurationManager.ConnectionStrings["projek.Properties.Settings.mahasiswa_dbConnectionString"].ConnectionString;

            // Buat koneksi ke database
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                // Query untuk menyimpan data ke dalam tabel order
                string query = "INSERT INTO [order] (order_akun, order_tanggal) VALUES (@akunId, @orderDate)";

                // Buat perintah SQL
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Tambahkan parameter untuk menghindari SQL injection
                    command.Parameters.AddWithValue("@akunId", akun_id);
                    command.Parameters.AddWithValue("@orderDate", orderDate);

                    // Eksekusi query
                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Order berhasil disimpan ke database.");
                        // Membuat instance form Akun
                        Detail_order detail_order = new Detail_order();

                        // Menampilkan form Produk
                        detail_order.Show();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Gagal menyimpan order ke database.");
                    }
                }
            }
        }

        private void akunBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.akunBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.mahasiswa_dbDataSet);

        }

        private void akunBindingNavigatorSaveItem_Click_1(object sender, EventArgs e)
        {
            this.Validate();
            this.akunBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.mahasiswa_dbDataSet);

        }

        private void OrderJual_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.akun' table. You can move, or remove it, as needed.
            this.akunTableAdapter.Fill(this.mahasiswa_dbDataSet.akun);

        }

        private void akunDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
