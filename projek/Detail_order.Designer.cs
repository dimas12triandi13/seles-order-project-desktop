﻿namespace projek
{
    partial class Detail_order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Detail_order));
            this.mahasiswa_dbDataSet = new projek.mahasiswa_dbDataSet();
            this.produkBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.produkTableAdapter = new projek.mahasiswa_dbDataSetTableAdapters.produkTableAdapter();
            this.tableAdapterManager = new projek.mahasiswa_dbDataSetTableAdapters.TableAdapterManager();
            this.produkBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.produkBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.produkComboBox = new System.Windows.Forms.ComboBox();
            this.jumlahNumber = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.simpanBtn = new System.Windows.Forms.Button();
            this.detail_orderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.detail_orderTableAdapter = new projek.mahasiswa_dbDataSetTableAdapters.detail_orderTableAdapter();
            this.detail_orderDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.orderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.orderTableAdapter = new projek.mahasiswa_dbDataSetTableAdapters.orderTableAdapter();
            this.orderComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mahasiswa_dbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.produkBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.produkBindingNavigator)).BeginInit();
            this.produkBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jumlahNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_orderBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_orderDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mahasiswa_dbDataSet
            // 
            this.mahasiswa_dbDataSet.DataSetName = "mahasiswa_dbDataSet";
            this.mahasiswa_dbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // produkBindingSource
            // 
            this.produkBindingSource.DataMember = "produk";
            this.produkBindingSource.DataSource = this.mahasiswa_dbDataSet;
            // 
            // produkTableAdapter
            // 
            this.produkTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.akunTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.detail_orderTableAdapter = this.detail_orderTableAdapter;
            this.tableAdapterManager.jualTableAdapter = null;
            this.tableAdapterManager.orderTableAdapter = this.orderTableAdapter;
            this.tableAdapterManager.produkTableAdapter = this.produkTableAdapter;
            this.tableAdapterManager.UpdateOrder = projek.mahasiswa_dbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // produkBindingNavigator
            // 
            this.produkBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.produkBindingNavigator.BindingSource = this.produkBindingSource;
            this.produkBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.produkBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.produkBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.produkBindingNavigatorSaveItem});
            this.produkBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.produkBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.produkBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.produkBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.produkBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.produkBindingNavigator.Name = "produkBindingNavigator";
            this.produkBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.produkBindingNavigator.Size = new System.Drawing.Size(800, 25);
            this.produkBindingNavigator.TabIndex = 0;
            this.produkBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // produkBindingNavigatorSaveItem
            // 
            this.produkBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.produkBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("produkBindingNavigatorSaveItem.Image")));
            this.produkBindingNavigatorSaveItem.Name = "produkBindingNavigatorSaveItem";
            this.produkBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.produkBindingNavigatorSaveItem.Text = "Save Data";
            this.produkBindingNavigatorSaveItem.Click += new System.EventHandler(this.produkBindingNavigatorSaveItem_Click);
            // 
            // produkComboBox
            // 
            this.produkComboBox.DataSource = this.produkBindingSource;
            this.produkComboBox.DisplayMember = "produk_nama";
            this.produkComboBox.FormattingEnabled = true;
            this.produkComboBox.Location = new System.Drawing.Point(177, 150);
            this.produkComboBox.Name = "produkComboBox";
            this.produkComboBox.Size = new System.Drawing.Size(146, 21);
            this.produkComboBox.TabIndex = 1;
            this.produkComboBox.ValueMember = "produk_id";
            this.produkComboBox.SelectedIndexChanged += new System.EventHandler(this.produkComboBox_SelectedIndexChanged);
            // 
            // jumlahNumber
            // 
            this.jumlahNumber.Location = new System.Drawing.Point(177, 192);
            this.jumlahNumber.Name = "jumlahNumber";
            this.jumlahNumber.Size = new System.Drawing.Size(146, 20);
            this.jumlahNumber.TabIndex = 2;
            this.jumlahNumber.ValueChanged += new System.EventHandler(this.jumlahNumber_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(98, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "produk";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(98, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Jumlah";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // simpanBtn
            // 
            this.simpanBtn.Location = new System.Drawing.Point(177, 237);
            this.simpanBtn.Name = "simpanBtn";
            this.simpanBtn.Size = new System.Drawing.Size(75, 23);
            this.simpanBtn.TabIndex = 5;
            this.simpanBtn.Text = "Simpan";
            this.simpanBtn.UseVisualStyleBackColor = true;
            this.simpanBtn.Click += new System.EventHandler(this.simpanBtn_Click);
            // 
            // detail_orderBindingSource
            // 
            this.detail_orderBindingSource.DataMember = "detail_order";
            this.detail_orderBindingSource.DataSource = this.mahasiswa_dbDataSet;
            // 
            // detail_orderTableAdapter
            // 
            this.detail_orderTableAdapter.ClearBeforeFill = true;
            // 
            // detail_orderDataGridView
            // 
            this.detail_orderDataGridView.AutoGenerateColumns = false;
            this.detail_orderDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detail_orderDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.detail_orderDataGridView.DataSource = this.detail_orderBindingSource;
            this.detail_orderDataGridView.Location = new System.Drawing.Point(421, 95);
            this.detail_orderDataGridView.Name = "detail_orderDataGridView";
            this.detail_orderDataGridView.Size = new System.Drawing.Size(342, 220);
            this.detail_orderDataGridView.TabIndex = 6;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "dorder_id";
            this.dataGridViewTextBoxColumn1.HeaderText = "dorder_id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "dorder_produk";
            this.dataGridViewTextBoxColumn2.HeaderText = "dorder_produk";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "dorder_jumlah";
            this.dataGridViewTextBoxColumn3.HeaderText = "dorder_jumlah";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 28.25F);
            this.label3.Location = new System.Drawing.Point(261, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(225, 44);
            this.label3.TabIndex = 7;
            this.label3.Text = "Detail Order";
            // 
            // orderBindingSource
            // 
            this.orderBindingSource.DataMember = "order";
            this.orderBindingSource.DataSource = this.mahasiswa_dbDataSet;
            // 
            // orderTableAdapter
            // 
            this.orderTableAdapter.ClearBeforeFill = true;
            // 
            // orderComboBox
            // 
            this.orderComboBox.DataSource = this.orderBindingSource;
            this.orderComboBox.DisplayMember = "order_id";
            this.orderComboBox.FormattingEnabled = true;
            this.orderComboBox.Location = new System.Drawing.Point(177, 110);
            this.orderComboBox.Name = "orderComboBox";
            this.orderComboBox.Size = new System.Drawing.Size(146, 21);
            this.orderComboBox.TabIndex = 8;
            this.orderComboBox.ValueMember = "order_id";
            this.orderComboBox.SelectedIndexChanged += new System.EventHandler(this.orderComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(98, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Master Order";
            // 
            // detail_order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.orderComboBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.detail_orderDataGridView);
            this.Controls.Add(this.simpanBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.jumlahNumber);
            this.Controls.Add(this.produkComboBox);
            this.Controls.Add(this.produkBindingNavigator);
            this.Name = "detail_order";
            this.Text = "detail_order";
            this.Load += new System.EventHandler(this.detail_order_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mahasiswa_dbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.produkBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.produkBindingNavigator)).EndInit();
            this.produkBindingNavigator.ResumeLayout(false);
            this.produkBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jumlahNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_orderBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.detail_orderDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.orderBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private mahasiswa_dbDataSet mahasiswa_dbDataSet;
        private System.Windows.Forms.BindingSource produkBindingSource;
        private mahasiswa_dbDataSetTableAdapters.produkTableAdapter produkTableAdapter;
        private mahasiswa_dbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator produkBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton produkBindingNavigatorSaveItem;
        private System.Windows.Forms.ComboBox produkComboBox;
        private System.Windows.Forms.NumericUpDown jumlahNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private mahasiswa_dbDataSetTableAdapters.detail_orderTableAdapter detail_orderTableAdapter;
        private System.Windows.Forms.Button simpanBtn;
        private System.Windows.Forms.BindingSource detail_orderBindingSource;
        private System.Windows.Forms.DataGridView detail_orderDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label label3;
        private mahasiswa_dbDataSetTableAdapters.orderTableAdapter orderTableAdapter;
        private System.Windows.Forms.BindingSource orderBindingSource;
        private System.Windows.Forms.ComboBox orderComboBox;
        private System.Windows.Forms.Label label4;
    }
}