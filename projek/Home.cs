﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projek
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }

        private void PenjualanBtn_Click(object sender, EventArgs e)
        {
            // Membuat instance form Penjualan
            Penjualan penjualanForm = new Penjualan();

            // Menampilkan form Penjualan
            penjualanForm.Show();
        }

        private void ProdukBtn_Click(object sender, EventArgs e)
        {
            // Membuat instance form Produk
            Produk produkForm = new Produk();

            // Menampilkan form Produk
            produkForm.Show();
        }

        private void AkunBtn_Click(object sender, EventArgs e)
        {
            // Membuat instance form Akun
            Akun akunForm = new Akun();

            // Menampilkan form Produk
            akunForm.Show();
        }

        private void OrderJualBtn_Click(object sender, EventArgs e)
        {
            // Membuat instance form Produk
            OrderJual orderJualForm = new OrderJual();

            // Menampilkan form Produk
            orderJualForm.Show();
        }
    }
}
