﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projek
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            // Membuat instance form Login
            Login loginForm = new Login();

            // Menampilkan form Login
            loginForm.Show();

            // Tutup form login
            //this.Close();
        }

        private void RegisBtn_Click(object sender, EventArgs e)
        {
            // Membuat instance form Login
            Regis regisForm = new Regis();

            // Menampilkan form Login
            regisForm.Show();
        }
    }
}
