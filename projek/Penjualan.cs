﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace projek
{
    public partial class Penjualan : Form
    {
        public Penjualan()
        {
            InitializeComponent();
        }

        private void orderBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.orderBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.mahasiswa_dbDataSet);

        }

        private void Penjualan_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.jual' table. You can move, or remove it, as needed.
            this.jualTableAdapter.Fill(this.mahasiswa_dbDataSet.jual);
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.akun' table. You can move, or remove it, as needed.
            this.akunTableAdapter.Fill(this.mahasiswa_dbDataSet.akun);
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.order' table. You can move, or remove it, as needed.
            this.orderTableAdapter.Fill(this.mahasiswa_dbDataSet.order);

        }

        private void orderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Dapatkan ID produk dari combo box
            int selectedOrderId = Convert.ToInt32(orderComboBox.SelectedValue);

            // Lakukan sesuatu dengan ID produk, misalnya tampilkan di MessageBox
            MessageBox.Show($"Selected ID: {selectedOrderId}");
        }

        private void akunComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Dapatkan ID produk dari combo box
            int selectedAkunId = Convert.ToInt32(akunComboBox.SelectedValue);
            // Lakukan sesuatu dengan ID produk, misalnya tampilkan di MessageBox
            MessageBox.Show($"Selected ID: {selectedAkunId}");
        }

        private void TanggalDtp_ValueChanged(object sender, EventArgs e)
        {

        }

        private void PembayaranTextField_TextChanged(object sender, EventArgs e)
        {

        }

        private void JualBtn_Click(object sender, EventArgs e)
        {
            DateTime jualDate = TanggalDtp.Value;
            String pembayaran = PembayaranTextField.Text;
            int selectedAkunId = Convert.ToInt32(akunComboBox.SelectedValue);
            int selectedOrderId = Convert.ToInt32(orderComboBox.SelectedValue);


            // Mendapatkan string koneksi dari konfigurasi
            string connectionString = ConfigurationManager.ConnectionStrings["projek.Properties.Settings.mahasiswa_dbConnectionString"].ConnectionString;

            // Buat koneksi ke database
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                // Query untuk menyimpan data ke dalam tabel order
                string query = "INSERT INTO [jual] (ojual_order, ojual_akun, ojual_tanggal,ojual_pembayaran) VALUES (@selectedOrderId, @selectedAkunId, @ojual_tanggal, @ojual_pembayaran)";

                // Buat perintah SQL
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Tambahkan parameter untuk menghindari SQL injection
                    command.Parameters.AddWithValue("@selectedOrderId", selectedOrderId);
                    command.Parameters.AddWithValue("@selectedAkunId", selectedAkunId);
                    command.Parameters.AddWithValue("@ojual_tanggal", jualDate);
                    command.Parameters.AddWithValue("@ojual_pembayaran", pembayaran);

                    // Eksekusi query
                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Order berhasil disimpan ke database.");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Gagal menyimpan order ke database.");
                    }
                }
            }
        }
    }
}
