﻿namespace projek
{
    partial class Regis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label akun_namaLabel;
            System.Windows.Forms.Label akun_emailLabel;
            System.Windows.Forms.Label akun_passLabel;
            this.mahasiswa_dbDataSet = new projek.mahasiswa_dbDataSet();
            this.akunBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.akunTableAdapter = new projek.mahasiswa_dbDataSetTableAdapters.akunTableAdapter();
            this.tableAdapterManager = new projek.mahasiswa_dbDataSetTableAdapters.TableAdapterManager();
            this.akun_namaTextBox = new System.Windows.Forms.TextBox();
            this.akun_emailTextBox = new System.Windows.Forms.TextBox();
            this.akun_passTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.registerCreate = new System.Windows.Forms.Button();
            akun_namaLabel = new System.Windows.Forms.Label();
            akun_emailLabel = new System.Windows.Forms.Label();
            akun_passLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mahasiswa_dbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.akunBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mahasiswa_dbDataSet
            // 
            this.mahasiswa_dbDataSet.DataSetName = "mahasiswa_dbDataSet";
            this.mahasiswa_dbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // akunBindingSource
            // 
            this.akunBindingSource.DataMember = "akun";
            this.akunBindingSource.DataSource = this.mahasiswa_dbDataSet;
            // 
            // akunTableAdapter
            // 
            this.akunTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.akunTableAdapter = this.akunTableAdapter;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.detail_orderTableAdapter = null;
            this.tableAdapterManager.jualTableAdapter = null;
            this.tableAdapterManager.orderTableAdapter = null;
            this.tableAdapterManager.produkTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = projek.mahasiswa_dbDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // akun_namaLabel
            // 
            akun_namaLabel.AutoSize = true;
            akun_namaLabel.Location = new System.Drawing.Point(275, 149);
            akun_namaLabel.Name = "akun_namaLabel";
            akun_namaLabel.Size = new System.Drawing.Size(39, 13);
            akun_namaLabel.TabIndex = 1;
            akun_namaLabel.Text = "nama :";
            // 
            // akun_namaTextBox
            // 
            this.akun_namaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.akunBindingSource, "akun_nama", true));
            this.akun_namaTextBox.Location = new System.Drawing.Point(348, 146);
            this.akun_namaTextBox.Name = "akun_namaTextBox";
            this.akun_namaTextBox.Size = new System.Drawing.Size(100, 20);
            this.akun_namaTextBox.TabIndex = 2;
            this.akun_namaTextBox.TextChanged += new System.EventHandler(this.akun_namaTextBox_TextChanged);
            // 
            // akun_emailLabel
            // 
            akun_emailLabel.AutoSize = true;
            akun_emailLabel.Location = new System.Drawing.Point(275, 191);
            akun_emailLabel.Name = "akun_emailLabel";
            akun_emailLabel.Size = new System.Drawing.Size(41, 13);
            akun_emailLabel.TabIndex = 3;
            akun_emailLabel.Text = "E-mail :";
            // 
            // akun_emailTextBox
            // 
            this.akun_emailTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.akunBindingSource, "akun_email", true));
            this.akun_emailTextBox.Location = new System.Drawing.Point(346, 188);
            this.akun_emailTextBox.Name = "akun_emailTextBox";
            this.akun_emailTextBox.Size = new System.Drawing.Size(100, 20);
            this.akun_emailTextBox.TabIndex = 4;
            this.akun_emailTextBox.TextChanged += new System.EventHandler(this.akun_emailTextBox_TextChanged);
            // 
            // akun_passLabel
            // 
            akun_passLabel.AutoSize = true;
            akun_passLabel.Location = new System.Drawing.Point(275, 236);
            akun_passLabel.Name = "akun_passLabel";
            akun_passLabel.Size = new System.Drawing.Size(59, 13);
            akun_passLabel.TabIndex = 5;
            akun_passLabel.Text = "Password :";
            // 
            // akun_passTextBox
            // 
            this.akun_passTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.akunBindingSource, "akun_pass", true));
            this.akun_passTextBox.Location = new System.Drawing.Point(348, 233);
            this.akun_passTextBox.Name = "akun_passTextBox";
            this.akun_passTextBox.Size = new System.Drawing.Size(100, 20);
            this.akun_passTextBox.TabIndex = 6;
            this.akun_passTextBox.TextChanged += new System.EventHandler(this.akun_passTextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(257, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 42);
            this.label1.TabIndex = 7;
            this.label1.Text = "Register Akun";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // registerCreate
            // 
            this.registerCreate.Location = new System.Drawing.Point(348, 280);
            this.registerCreate.Name = "registerCreate";
            this.registerCreate.Size = new System.Drawing.Size(75, 23);
            this.registerCreate.TabIndex = 8;
            this.registerCreate.Text = "Register";
            this.registerCreate.UseVisualStyleBackColor = true;
            this.registerCreate.Click += new System.EventHandler(this.registerCreate_Click);
            // 
            // Regis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.registerCreate);
            this.Controls.Add(this.label1);
            this.Controls.Add(akun_passLabel);
            this.Controls.Add(this.akun_passTextBox);
            this.Controls.Add(akun_emailLabel);
            this.Controls.Add(this.akun_emailTextBox);
            this.Controls.Add(akun_namaLabel);
            this.Controls.Add(this.akun_namaTextBox);
            this.Name = "Regis";
            this.Text = "Regis";
            this.Load += new System.EventHandler(this.Regis_Load);
            ((System.ComponentModel.ISupportInitialize)(this.mahasiswa_dbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.akunBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private mahasiswa_dbDataSet mahasiswa_dbDataSet;
        private System.Windows.Forms.BindingSource akunBindingSource;
        private mahasiswa_dbDataSetTableAdapters.akunTableAdapter akunTableAdapter;
        private mahasiswa_dbDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox akun_namaTextBox;
        private System.Windows.Forms.TextBox akun_emailTextBox;
        private System.Windows.Forms.TextBox akun_passTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button registerCreate;
    }
}