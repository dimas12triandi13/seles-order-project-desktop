﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projek
{
    public partial class Akun : Form
    {
        public Akun()
        {
            InitializeComponent();
        }

        private void akunBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.akunBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.mahasiswa_dbDataSet);

        }

        private void Akun_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.akun' table. You can move, or remove it, as needed.
            this.akunTableAdapter.Fill(this.mahasiswa_dbDataSet.akun);

        }

        private void akunDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
