﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace projek
{
    public partial class Detail_order : Form
    {
        public Detail_order()
        {
            InitializeComponent();
        }

        private void produkBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.produkBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.mahasiswa_dbDataSet);

        }

        private void detail_order_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.order' table. You can move, or remove it, as needed.
            this.orderTableAdapter.Fill(this.mahasiswa_dbDataSet.order);
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.detail_order' table. You can move, or remove it, as needed.
            this.detail_orderTableAdapter.Fill(this.mahasiswa_dbDataSet.detail_order);
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.produk' table. You can move, or remove it, as needed.
            this.produkTableAdapter.Fill(this.mahasiswa_dbDataSet.produk);

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void produkComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Dapatkan ID produk dari combo box
            int selectedProductId = Convert.ToInt32(produkComboBox.SelectedValue);

            // Lakukan sesuatu dengan ID produk, misalnya tampilkan di MessageBox
            MessageBox.Show($"Selected Product ID: {selectedProductId}");
        }

        private void jumlahNumber_ValueChanged(object sender, EventArgs e)
        {

        }

        private void simpanBtn_Click(object sender, EventArgs e)
        {
            // Dapatkan ID produk dari combo box
            int selectedProductId = Convert.ToInt32(produkComboBox.SelectedValue);

            // Dapatkan nilai jumlah
            int jumlah = Convert.ToInt32(jumlahNumber.Value);

            int selectedOrderId = Convert.ToInt32(produkComboBox.SelectedValue);

            // Lakukan sesuatu dengan ID produk dan jumlah, misalnya simpan ke database
            //SimpanDetailOrder(selectedProductId, jumlah);

            //string akun_id = textBox1.Text;

            // Mendapatkan string koneksi dari konfigurasi
            string connectionString = ConfigurationManager.ConnectionStrings["projek.Properties.Settings.mahasiswa_dbConnectionString"].ConnectionString;

            // Buat koneksi ke database
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                // Query untuk menyimpan data ke dalam tabel order
                string query = "INSERT INTO [detail_order] (dorder_produk, dorder_jumlah, dorder_master) VALUES (@selectedProductId, @dorder_jumlah, @dorder_master)";

                // Buat perintah SQL
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Tambahkan parameter untuk menghindari SQL injection
                    command.Parameters.AddWithValue("@selectedProductId", selectedProductId);
                    command.Parameters.AddWithValue("@dorder_jumlah", jumlah);
                    command.Parameters.AddWithValue("@dorder_master", selectedOrderId);

                    // Eksekusi query
                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Order berhasil disimpan ke database.");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Gagal menyimpan order ke database.");
                    }
                }
            }
        }

      

        private void orderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Dapatkan ID produk dari combo box
            int selectedOrderId = Convert.ToInt32(produkComboBox.SelectedValue);

            // Lakukan sesuatu dengan ID produk, misalnya tampilkan di MessageBox
            MessageBox.Show($"Selected Order ID: {selectedOrderId}");
        }
    }
}
