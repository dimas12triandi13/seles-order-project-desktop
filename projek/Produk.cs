﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace projek
{
    public partial class Produk : Form
    {
        public Produk()
        {
            InitializeComponent();
        }

        private void produkBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.produkBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.mahasiswa_dbDataSet);

        }

        private void Produk_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.produk' table. You can move, or remove it, as needed.
            this.produkTableAdapter.Fill(this.mahasiswa_dbDataSet.produk);

        }

        private void produk_nama_textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void produk_harga_textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void produk_jumlah_textBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Ambil data dari input form
            string namaProduk = produk_nama_textBox.Text;
            int hargaProduk;
            int jumlahProduk;

            // Validasi input harga
            if (!int.TryParse(produk_harga_textBox.Text, out hargaProduk))
            {
                MessageBox.Show("Masukkan harga yang valid.");
                return;
            }

            // Validasi input jumlah
            if (!int.TryParse(produk_jumlah_textBox.Text, out jumlahProduk))
            {
                MessageBox.Show("Masukkan jumlah yang valid.");
                return;
            }
            string connectionString = ConfigurationManager.ConnectionStrings["projek.Properties.Settings.mahasiswa_dbConnectionString"].ConnectionString;

            // Buat koneksi ke database
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                // Query untuk menyimpan data ke database
                string query = "INSERT INTO produk (produk_nama, produk_harga, produk_jumlah) VALUES (@nama, @harga, @jumlah)";

                // Buat perintah SQL
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Tambahkan parameter untuk menghindari SQL injection
                    command.Parameters.AddWithValue("@nama", namaProduk);
                    command.Parameters.AddWithValue("@harga", hargaProduk);
                    command.Parameters.AddWithValue("@jumlah", jumlahProduk);

                    // Eksekusi query
                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Data berhasil disimpan ke database.");

                        // Refresh DataGridView untuk menampilkan data terbaru
                        this.produkTableAdapter.Fill(this.mahasiswa_dbDataSet.produk);
                    }
                    else
                    {
                        MessageBox.Show("Gagal menyimpan data ke database.");
                    }
                }
            }
        }

        private void produkDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
