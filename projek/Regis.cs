﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace projek
{
    public partial class Regis : Form
    {
        public Regis()
        {
            InitializeComponent();
        }

        private void akunBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.akunBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.mahasiswa_dbDataSet);

        }

        private void Regis_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'mahasiswa_dbDataSet.akun' table. You can move, or remove it, as needed.
            this.akunTableAdapter.Fill(this.mahasiswa_dbDataSet.akun);

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void registerCreate_Click(object sender, EventArgs e)
        {
            // Ambil data dari input form
            string nama = akun_namaTextBox.Text;
            string email = akun_emailTextBox.Text;
            string password = akun_passTextBox.Text;

            string connectionString = ConfigurationManager.ConnectionStrings["projek.Properties.Settings.mahasiswa_dbConnectionString"].ConnectionString;

            // Buat koneksi ke database
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                // Query untuk menyimpan data ke database
                string query = "INSERT INTO akun (akun_nama, akun_email, akun_pass) VALUES (@nama, @email, @password)";

                // Buat perintah SQL
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    // Tambahkan parameter untuk menghindari SQL injection
                    command.Parameters.AddWithValue("@nama", nama);
                    command.Parameters.AddWithValue("@email", email);
                    command.Parameters.AddWithValue("@password", password);

                    // Jalankan perintah SQL
                    int rowsAffected = command.ExecuteNonQuery();

                    if (rowsAffected > 0)
                    {
                        MessageBox.Show("Data berhasil disimpan ke database.");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Gagal menyimpan data ke database.");
                    }
                }
            }
        }

        private void akun_passTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void akun_emailTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void akun_namaTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
